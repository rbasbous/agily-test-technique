## Summary
```
The meteo application is divided into two parts
 - The front-end 
 - The back-end
 
The front-end is developed with Vue.js (Version 3) (3 hours)
The Back-end is developed with Express ( 2 hours)

To launch the application, check the Readme files in check directory (meteo-api and meteo-app)
```

# Preferred Part for me
```
The prefered part for me is the back-end, I like the logical and algorithmical part that is developed mainly
(not only in this project) in the back-end services.
Besides, I like also the front-end part when it comes to application infrastructure, logic and complex algorithms.
```

## Difficulties
```
The concept of the project is easy, but the timing was difficult where I tried hardly to develop the application
at night after I come back from work.
Also, another complex part was the CSS (style), it is a part that I'm not comfortable
with (it can be seen in the desing of the application)
```

## Application Functionality
```
The home page is the page of which the user select a county

Workflow:
 - The user enter the name of the city of which he wants to check it's weather
 - If the name of the city was correct, then he wil get up to 5 suggestions. (Cities among the word
 with identical names)
 - After selecting a city, he will be transfered to a page displaying the results of the weather of coming 7 days
 - The selected city is stored in the localstorage, so if he quit the page and then come back he can still see 
 the weather information directly of the city that was selected if he access directly the page (/results)
 - If the user try to access the page (/result) without selecting a city (localstorage empty), he will be redirected
 to the home page to select a city
 - A spinner was used and integraded inside the (response, request interceptors) to notify the user
 by the going HTTP requests
 - A 404 Page also was taken into consideration  
``` 

## Things that I would like to improve
```
I've created some interfaces and DTOs that are used in the application and the API. I've duplicated those
interfaces and DTOs. If I got more time, I'd like to create a local library shared by the front and the back
so in this way we avoid duplicating code in both sides (front and back).
```
