import express, { Express, Request, Response } from 'express';
import dotenv from 'dotenv';
import helmet from "helmet";
import cors from 'cors';
import WeatherRouter from "./src/routes/WeatherRouter";

dotenv.config();

const app: Express = express();
const port = process.env.PORT;

//Secure API and prevent Cross Site Scripting
app.use(helmet());

// Enable Cors
app.use(cors({
  origin: process.env.ORIGIN
}))

app.get('/', (req: Request, res: Response) => {
  res.send('Hello World !! Welcome to the express server');
});


app.use('/weather', WeatherRouter)

app.listen(port, () => {
  console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
});
