import * as AxiosService from '../Axios'
import { HttpMethodsEnums } from "../../common/enums";
import { ICity, IGeoApiCallParams, IWeatherData, IWeatherDataApiCallParams } from "../../common/interfaces";
import { WorkDone } from '../../common/dtos';
import NodeCache from "node-cache";
import { IDailyWeatherData, IFormattedWeatherData } from "../../common/interfaces/open-weather-map.dto";

const nodeCache = new NodeCache();

const WeatherService = {
  async getCitiesPropositions(city: string): Promise<WorkDone<ICity[]>> {
    const params: IGeoApiCallParams = {
      q: city ,
      limit: 5,
      appid: process.env.OPEN_WEATHER_API_KEY
    };
    const wd: WorkDone<ICity[]> = await AxiosService.doHttpCall<IGeoApiCallParams, ICity[]>(
      `${process.env.OPEN_WEATHER_API_URL}/geo/1.0/direct`,
      HttpMethodsEnums.GET,
      undefined,
      params
    )
    return wd;
  },
  async getWeatherData(lat: number, lon: number): Promise<WorkDone<any>> {
    const params: IWeatherDataApiCallParams = {
      lat: lat,
      lon: lon,
      exclude: 'current,hourly,minutely',
      appid: process.env.OPEN_WEATHER_API_KEY,
      units: 'metric'
    }
    const targetWeatherData = this.getWeatherDataFromNodeCache(lat, lon);
    if(targetWeatherData) return WorkDone.buildOk(targetWeatherData);
    const wd: WorkDone<IWeatherData> = await AxiosService.doHttpCall<IWeatherDataApiCallParams, IWeatherData>(
      `${process.env.OPEN_WEATHER_API_URL}/data/2.5/onecall`,
      HttpMethodsEnums.GET,
      undefined,
      params
    )
    if(wd.isOk) {
      // Add the results to the node cache
      const today = new Date();
      const todayS = this.convertDateToString(today);
      const formattedWeatherData = this.prepareWeatherData(wd.data as IWeatherData);
      if(nodeCache.has(todayS)) {
        // There are already search done today
        const formattedWeatherDatas: IFormattedWeatherData[] = nodeCache.take(todayS) as IFormattedWeatherData[];
        formattedWeatherDatas.push(formattedWeatherData)
        nodeCache.set(todayS, formattedWeatherDatas)
      } else {
        nodeCache.set(todayS, [formattedWeatherData])
      }
      return WorkDone.buildOk(formattedWeatherData)
    }
    return wd;
  },
  prepareWeatherData(weatherData: IWeatherData): IFormattedWeatherData {
    const formattedWeatherData: IFormattedWeatherData = {
      lat: weatherData.lat,
      lon: weatherData.lon,
      daily: []
    };
    const dailyWeatherData: IDailyWeatherData[] = weatherData.daily;
    if(dailyWeatherData && dailyWeatherData.length > 0) {
      dailyWeatherData.forEach((data: IDailyWeatherData) => {
        formattedWeatherData.daily.push({
          dt: data.dt,
          dayTemp: data.temp.day,
          nightTemp: data.temp.night,
          humidity: data.humidity,
          pressure: data.pressure,
          windSpeed: data.wind_speed
        })
      })
    }
    return formattedWeatherData;
  },
  convertDateToString(date: Date): string {
    return `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`;
  },

  getWeatherDataFromNodeCache(lat: number, lon: number): IFormattedWeatherData|undefined {
    const today = new Date();
    const todayS = this.convertDateToString(today);
    // If there exists results for today, check among those results if there exist weather data for the coming 7 day for the specified lon and lat
    if(nodeCache.has(todayS)) {
      const weatherData: IFormattedWeatherData[] = nodeCache.get(todayS) as IFormattedWeatherData[];
      if(weatherData && weatherData.length > 0) {
        const targetWeatherData: IFormattedWeatherData|undefined = weatherData.find((data: IFormattedWeatherData) =>
          Math.round(data.lon) == Math.round(lon) && Math.round(data.lat) == Math.round(lat)
        )
        return targetWeatherData
      }
    }
  }
}

export default WeatherService;
