import { Request, Response } from "express";
import WeatherService from './service';
import { IWeatherData } from '../../common/interfaces'
import { WorkDone } from '../../common/dtos'

export async function getCitiesPropositions(req: Request, res: Response): Promise<void> {
  const city = req.query.city as string;
  if(!city) res.status(400).send(WorkDone.buildError('City name is not provided'));
  const wd = await WeatherService.getCitiesPropositions(city);
  res.status(200).send(wd);
}

export async function getWeatherData(req: Request, res: Response): Promise<void> {
  const lat = parseFloat(req.query.lat as string);
  const lon = parseFloat(req.query.lon as string);
  let wd: WorkDone<IWeatherData>;

  if (isNaN(lat) || isNaN(lon)) {
    wd = WorkDone.buildError('Error in data format');
    res.status(400).send(wd)
  }
  wd = await WeatherService.getWeatherData(lat, lon);
  res.status(200).send(wd)
}
