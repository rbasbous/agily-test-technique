import axios, { AxiosError, AxiosResponse } from "axios";
import { HttpMethodsEnums } from "../../common/enums";
import { WorkDone } from "../../common/dtos";

export function doHttpCall<T, P>(url: string, method: HttpMethodsEnums, data?: T, params?: T): Promise<WorkDone<P>> {
  return axios({
    url: url,
    method: method,
    data: data,
    params: params
  }).then((response: AxiosResponse) => {
    return WorkDone.buildOk(response.data);
  }).catch((error: AxiosError) => {
    return WorkDone.buildError(error.message.toString());
  });
}
