export interface ICity {
  name: string;

  lat: number;
  lon: number;
  country: string;
  state?: string
}

export interface IGeoApiCallParams {
  q: string;
  limit: number;
  appid?: string
}

export interface IWeatherDataApiCallParams {
  lat: number;
  lon: number;
  exclude?: string;
  units?: string;
  appid?: string;
}

export interface IWeatherData {
  lon: number;
  lat: number;
  daily: IDailyWeatherData[];
}

export interface IDailyWeatherData {
  dt: number;
  temp: ITempDetails;
  wind_speed: number;
  humidity: number;
  pressure: number;
}

export interface ITempDetails {
  day: number;
  night: number;
}

export interface IFormattedDailyWeatherData {
  dt: number;
  dayTemp: number;
  nightTemp: number;
  humidity: number;
  windSpeed: number;
  pressure: number;
}

export interface IFormattedWeatherData {
  lat: number;
  lon: number;
  daily: IFormattedDailyWeatherData[];
}
