import {
  ICity,
  IGeoApiCallParams,
  IWeatherDataApiCallParams,
  IWeatherData
} from "./open-weather-map.dto";

export {
  IWeatherData,
  ICity,
  IWeatherDataApiCallParams,
  IGeoApiCallParams
}
