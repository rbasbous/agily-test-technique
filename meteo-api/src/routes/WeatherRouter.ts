import { Router } from 'express'
import { WeatherComponent } from '../components'

const router = Router();

router.get('/cities-propositions', WeatherComponent.getCitiesPropositions)

router.get('/weather-data', WeatherComponent.getWeatherData)

export default router;
