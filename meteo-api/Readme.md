# meteo-api

## Project setup
```
yarn install
```

### To run the project in development with the possibility to watch files changes run
```
yarn dev
```

### To run the project in development without watching files changes (More stable)
```
 - yarn build
 - yarn start
```
