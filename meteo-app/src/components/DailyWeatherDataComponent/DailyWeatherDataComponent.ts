import { defineComponent, computed } from 'vue'
import { CalendarConstants } from '@/shared/constants'

export default defineComponent({
  name: 'DailyWeatherDataComponent',
  props: {
    dailyWeatherData: Object,
    isDisplayDetails: Boolean
  },
  emits: ['displayWeatherDetails'],
  setup (props, { emit }) {
    const dayText = computed(() => {
      if (props.dailyWeatherData && props.dailyWeatherData.dt) {
        return convertTimeStampToDate(props.dailyWeatherData.dt)
      }
      return null
    })

    function convertTimeStampToDate (timeStamp: number) {
      const date = new Date(timeStamp * 1000)
      const dayName = CalendarConstants.DayConstants.find(item => item.value === date.getDay())?.text
      const monthName = CalendarConstants.MonthConstants.find(item => item.value === date.getMonth())?.text

      return `${dayName} le ${date.getDate()} ${monthName}`
    }

    function displayWeatherDetails () {
      emit('displayWeatherDetails', props.dailyWeatherData)
    }

    return {
      displayWeatherDetails,
      dayText
    }
  }
})
