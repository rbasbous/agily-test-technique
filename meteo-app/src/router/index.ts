import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import SearchView from '../views/SearchView/SearchView.vue'
import ResultsView from '../views/ResultsView/ResultsView.vue'
import PageNotFoundView from '../views/PageNotFoundView/PageNotFoundView.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'search',
    component: SearchView
  },
  {
    path: '/results',
    name: 'results',
    component: ResultsView
  },
  {
    path: '/:catchAll(.*)',
    component: PageNotFoundView
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
