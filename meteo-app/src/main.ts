import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { AxiosService } from '@/services/axios.service'
import { WeatherService } from '@/services/weather.service'

const axiosService = new AxiosService()
const weatherService = new WeatherService(axiosService)

const app = createApp(App)
app.use(store)
app.use(router)
app.provide('weatherService', weatherService)
app.mount('#app')
