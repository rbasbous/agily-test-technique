import { IRequestsState } from '@/store/state'

export const increment = (state: IRequestsState): void => {
  state.requestsCount++
}

export const decrement = (state: IRequestsState): void => {
  state.requestsCount--
}
