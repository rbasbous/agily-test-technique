import { IRequestsState } from '@/store/state'

export const requestsCount = (state: IRequestsState): number => state.requestsCount
