import { AxiosService } from '@/services/axios.service'
import { WorkDone } from '@/shared/dtos'
import { ICity, IFormattedWeatherData } from '@/shared/interfaces'

export class WeatherService {
  private readonly apiUrl = `${process.env.VUE_APP_API_URL}/weather`;
  private axiosService: AxiosService

  constructor (axiosService: AxiosService) {
    this.axiosService = axiosService
  }

  public async getCitiesList (cityName: string): Promise<WorkDone<ICity[]>> {
    return this.axiosService.doHttpCalls<ICity[]>(
      () => this.axiosService.axiosInstance.get(`${this.apiUrl}/cities-propositions`, { params: { city: cityName } })
    )
  }

  public async getWeatherData (lon: number, lat: number): Promise<WorkDone<IFormattedWeatherData>> {
    return this.axiosService.doHttpCalls<IFormattedWeatherData>(
      () => this.axiosService.axiosInstance.get(`${this.apiUrl}/weather-data`, { params: { lon: lon, lat: lat } })
    )
  }

  public setSelectedCityLocalStorage (city: ICity) {
    localStorage.setItem('selectedCity', JSON.stringify(city))
  }

  public getSelectedCityLocalStorage (): ICity|undefined {
    const selectedCityS = localStorage.getItem('selectedCity')
    if (!selectedCityS) return undefined
    return JSON.parse(selectedCityS)
  }
}
