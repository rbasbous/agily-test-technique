import {
  ICity,
  IGeoApiCallParams,
  IWeatherDataApiCallParams,
  IFormattedWeatherData,
  IFormattedDailyWeatherData
} from "./open-weather-map.dto";

export {
  ICity,
  IWeatherDataApiCallParams,
  IGeoApiCallParams,
  IFormattedWeatherData,
  IFormattedDailyWeatherData
}
