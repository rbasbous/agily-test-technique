export interface ICity {
  name: string;
  local_names: object;
  lat: number;
  lon: number;
  country: string;
  state?: string
}

export interface IGeoApiCallParams {
  q: string;
  limit: number;
  appid?: string
}

export interface IWeatherDataApiCallParams {
  lat: number;
  lon: number;
  exclude?: string;
  appid?: string;
}

export interface IFormattedDailyWeatherData {
  dt: number;
  dayTemp: number;
  nightTemp: number;
  humidity: number;
  windSpeed: number;
  pressure: number;
}

export interface IFormattedWeatherData {
  lat: number;
  lon: number;
  daily: IFormattedDailyWeatherData[];
}
