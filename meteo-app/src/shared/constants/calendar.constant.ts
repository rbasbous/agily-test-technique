export const MonthConstants = [
  { text: 'Janvier', value: 0 },
  { text: 'Février', value: 1 },
  { text: 'Mars', value: 2 },
  { text: 'Avril', value: 3 },
  { text: 'Mai', value: 4 },
  { text: 'Juin', value: 5 },
  { text: 'Juillet', value: 6 },
  { text: 'Août', value: 7 },
  { text: 'Septembre', value: 8 },
  { text: 'Octobre', value: 9 },
  { text: 'Novombre', value: 10 },
  { text: 'Décembre', value: 11 }
]

export const DayConstants = [
  { text: 'Dimanche', value: 0 },
  { text: 'Lundi', value: 1 },
  { text: 'Mardi', value: 2 },
  { text: 'Mercredi', value: 3 },
  { text: 'Jeudi', value: 4 },
  { text: 'Vendredi', value: 5 },
  { text: 'Samedi', value: 6 }
]
