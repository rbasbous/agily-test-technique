import { defineComponent, inject, onBeforeMount, ref } from 'vue'
import { ICity, IFormattedDailyWeatherData, IFormattedWeatherData } from '@/shared/interfaces'
import { WeatherService } from '@/services/weather.service'
import router from '@/router'
import { WorkDone } from '@/shared/dtos'
import DailyWeatherDataComponent from '@/components/DailyWeatherDataComponent/DailyWeatherDataComponent.vue'

export default defineComponent({
  name: 'ResultsView',
  components: { DailyWeatherDataComponent },
  setup () {
    const weatherService = inject('weatherService') as WeatherService
    const formattedDailyWeatherData = ref<IFormattedDailyWeatherData[]>([])
    const selectedWeatherInfo = ref<IFormattedDailyWeatherData>()
    const selectedCity = ref<ICity|undefined>()

    onBeforeMount(async () => {
      selectedCity.value = weatherService.getSelectedCityLocalStorage()
      if (selectedCity.value) {
        // get the weather info for the selected city
        const wd: WorkDone<IFormattedWeatherData> = await weatherService.getWeatherData(selectedCity.value.lon, selectedCity.value.lat)
        if (wd.isOk && wd.data) {
          formattedDailyWeatherData.value = wd.data.daily
          selectedWeatherInfo.value = wd.data.daily[0]
        }
      } else {
        await router.push({ name: 'search' })
      }
    })

    function displaySelectedWeatherDetails (weatherInfo: IFormattedDailyWeatherData) {
      selectedWeatherInfo.value = weatherInfo
    }

    return {
      formattedDailyWeatherData,
      selectedWeatherInfo,
      selectedCity,
      displaySelectedWeatherDetails
    }
  }
})
