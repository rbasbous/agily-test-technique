import { defineComponent, inject, ref } from 'vue'
import { WeatherService } from '@/services/weather.service'
import { ICity } from '@/shared/interfaces'
import router from '@/router'

export default defineComponent({
  name: 'SearchView',
  setup () {
    const city = ref<string>('')
    const weatherService = inject('weatherService') as WeatherService
    const citiesList = ref<ICity[]>([])

    async function submitForm () {
      const wd = await weatherService.getCitiesList(city.value)
      if (wd && wd.isOk && wd.data && wd.data.length > 0) {
        citiesList.value = wd.data as ICity[]
      }
    }

    async function selectCity (selectedCity: ICity) {
      weatherService.setSelectedCityLocalStorage(selectedCity)
      await router.push({ name: 'results' })
    }

    return {
      city,
      citiesList,
      submitForm,
      selectCity
    }
  }
})
